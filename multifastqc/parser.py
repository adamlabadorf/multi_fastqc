import pandas as pd

def parse_fastqc_data(fn) :
  """Parse through a fastqc output file and return a dictionary where keys are
  module names and values are pandas Dataframes with the corresponding data"""
  modules = {}
  with open(fn) as f :
    curr_module = None
    for r in f :
      if r.startswith("##") : continue
      if r.startswith(">>END_MODULE") : # end module
        status = modules[curr_module].status

        # by default pandas reads all columns as strings
        # use pd.to_numeric on each column to cast into numeric
        # types when possible
        modules[curr_module] = modules[curr_module].apply(
          pd.to_numeric,errors='ignore'
        )
        modules[curr_module].status = status
      elif r.startswith(">>") : # begin module

        # module start line looks like
        # >>(name of module)	(pass|fail)
        curr_module, status = r[2:].strip().split("\t")

        # special case for Sequence Duplication Levels
        if curr_module == "Sequence Duplication Levels" :
          _, dedup_level = next(f)[1:].strip().split("\t")

        # normally the first commented line of the module is the header
        header = next(f)[1:].strip().replace(' ','_').split("\t")
        modules[curr_module] = pd.DataFrame(columns=header)
        modules[curr_module].status = status

      else : # current module data

        # skip Per tile sequence quality, since it's not really useful
        # and many data points
        if curr_module == "Per tile sequence quality" : continue

        # At a row in the file that is a row of data, add that row to a
        # growing DataFrame for the current module
        modules[curr_module].loc[modules[curr_module].shape[0]] = r.strip().split("\t")

  return modules

def parse_summary(fn) :
  """Parse a fastqc summary.txt file. Returns a pandas Series object where
  the index is the summary module name and the value is "PASS", "FAIL", or
  "WARN" strings as appropriate."""
  summary_df = pd.read_csv(fn
    ,index_col=1
    ,names=('status','fn')
    ,delimiter="\t"
  )

  return summary_df.status



