import base64
import io
import warnings

import matplotlib
matplotlib.use("agg")

import matplotlib.patches as patches
import matplotlib.pyplot as mp
from pylab import arange
import numpy as np

import pandas as pd

from multifastqc import METRICS

# base Plotter class
class Plotter(object) :
  def __init__(self) :
    warnings.warn("The base Plotter object does nothing, use a subclass")
  def summary(self,*args,**kwargs) : pass
  def library_size(self,*args,**kwargs) : pass
  def basic_statistics(self,*args,**kwargs) : pass
  def per_base_sequence_quality(self,*args,**kwargs) : pass
  def per_tile_sequence_quality(self,*args,**kwargs) : pass
  def per_sequence_quality_scores(self,*args,**kwargs) : pass
  def per_base_sequence_content(self,*args,**kwargs) : pass
  def per_sequence_gc_content(self,*args,**kwargs) : pass
  def per_base_n_content(self,*args,**kwargs) : pass
  def sequence_length_distribution(self,*args,**kwargs) : pass
  def sequence_duplication_levels(self,*args,**kwargs) : pass
  def overrepresented_sequences(self,*args,**kwargs) : pass
  def adapter_content(self,*args,**kwargs) : pass
  def kmer_content(self,*args,**kwargs) : pass

### Translate a matplotlib mp.figure into an html image tag with base64 encoding.
def fig_to_b64_img(fig) :
  buf = io.BytesIO()
  fig.savefig(buf,format="png")
  buf.seek(0)
  b64 = base64.b64encode(buf.read()).decode('utf-8')
  return '<img src="data:image/png;base64,{}"></img>'.format(b64)

# matplotlib has a problem plotting objects with numpy.float64 objects
# this function is a convenience function that explicitly casts all
# items of the argument to python floats
fl = lambda x: [float(_) for _ in x]

class MatplotlibPlotter(object) :

  def __init__(self,summary,modules) :

    self.report_data = {}

    self.summary_df = summary
    self.modules = modules
    self.sorted_modules = sorted(modules.items())
    self.fastqc_dirs, self.fastqc_modules = zip(*self.sorted_modules)


  def summary(self,figsize=(12,8)) :

    summary = self.summary_df

    row_width = 10  ### Rectangle width
    num_samples = summary.shape[1]
    num_modules = summary.shape[0]
    fig_height = row_width*float(num_samples)/90 + 3 

    f = mp.figure(figsize=(12,fig_height))
    ax = f.gca()
#    ax.figsize=(12,fig_height)
    ### Loop over sample names (columns in summary DataFrame)
    for i in range(num_samples) :
      ### Loop over stats (12 rows in summary DataFrame)
      for j in range(num_modules) :
        ### If on the last iteration of samples, add stat names
        if i == num_samples-1 :
           ax.text(j*row_width  ### Define horizontal location
            ,row_width*num_samples ### Define vertical location
            ,summary.index[j]  ### Define text content (sample name)
            ,rotation=90  ### Orientation
            ,va='bottom'  ### Vertical and horizontal alignment
            ,ha='left'
            )
        ### If on the last iteration of stats, add fastqc sample names
        if j == num_modules-1 :
          ax.text(row_width*num_modules  ### Define horizontal location
            ,i*row_width+row_width/2  ### Define vertical location
            ,summary.columns[i],  ### Define text content (stat name)
            verticalalignment='center')

        s = summary.ix[j,i]
        c = 'w'
        if s == 'WARN' :
          c = 'y'
        elif s == 'FAIL' :
          c = 'r'
        ax.add_patch(
          patches.Rectangle(
            (j*row_width,i*row_width)
            ,row_width,row_width
            ,facecolor=c
          )
        )
    #ax.axis("tight")
    ax.axis("off")
    ax.axis([0,num_modules*row_width+12*row_width,0,num_samples*row_width+16*row_width])

    self.report_data["summary"] = fig_to_b64_img(f)
    return self.report_data["summary"]

  def library_size(self):
    met = "Basic Statistics"
    
    num_samples = len(self.sorted_modules)
    bar_width = 8
    ### Figure height
    fig_height = bar_width*float(num_samples)/90 + 3
    ### Bar positions
    pos = arange(start=0,stop=num_samples*bar_width,step = bar_width) + 0.5
    f = mp.figure(figsize=(13,fig_height))
    ax = f.gca()

    samples = []
    library = []
    for samp,v in self.sorted_modules :
      library.append(int(v[met].Value[3]))
      samples.append(samp)
    ax.barh(bottom=pos,width=library, align='center', log = True)
    ax.set_yticks(pos)
    ax.set_yticklabels(samples)
    ax.set_xlabel("Library size")
     
    self.report_data["library_size"] = fig_to_b64_img(f)
    return self.report_data["library_size"]

  # plots of individual modules
  # Per base sequence quality
  def per_base_sequence_quality(self) :
    f = mp.figure()
    ax = f.gca()
    xlabs = self.fastqc_modules[0]["Per base sequence quality"].Base
    x = fl(np.arange(len(xlabs)))
    ax.set_xticks(x)
    ax.set_xticklabels(xlabs,rotation=90)
    for samp,v in self.sorted_modules :
      ls = '-' if v["Per base sequence quality"].status == 'pass' else '--'
      ax.plot(x,fl(v["Per base sequence quality"].Mean),ls)
    ax.set_title("Per base sequence quality")
    ax.set_ylabel('Phred score')
    ax.set_xlabel('Position in read (bp)')

    ax.grid()
    f.subplots_adjust(bottom=0.19)
    self.report_data["per_base_qual"] = fig_to_b64_img(f)
    return self.report_data["per_base_qual"]

  # Per sequence quality scores
  def per_sequence_quality_scores(self) :
    met = "Per sequence quality scores"
    f = mp.figure()
    ax = f.gca()
    x = np.arange(40)
    ax.set_xticks(x)
    ax.set_xticklabels(x,rotation=90)
    ax.set_yscale('log')
    for samp,v in self.sorted_modules :
      ls = '-' if v[met].status == 'pass' else '--'
      ax.plot(fl(v[met].Quality),fl(v[met].Count),ls)
    ax.set_title("Per sequence quality scores")
    ax.set_ylabel('Read count')
    ax.set_xlabel('Mean sequence quality (Phred score)')

    ax.grid()
    self.report_data["per_seq_qual"] = fig_to_b64_img(f)
    return self.report_data["per_seq_qual"]

  # Per base sequence content
  def per_base_sequence_content(self) :
    f = mp.figure()
    ax = f.gca()
    xlabs = self.fastqc_modules[0]["Per base sequence content"].Base
    x = fl(np.arange(len(xlabs)))
    ax.set_xticks(x)
    ax.set_xticklabels(xlabs,rotation=90)

    # calculate the average GATC composition across all samples
    avg_cont = pd.DataFrame(0
      ,index=self.fastqc_modules[0]["Per base sequence content"].index
      ,columns=self.fastqc_modules[0]["Per base sequence content"].columns[1:]
    )
    for fastqc1 in self.fastqc_modules :
      avg_cont = avg_cont.add(fastqc1["Per base sequence content"][avg_cont.columns]/len(self.fastqc_modules))
      #for fastqc2 in fastqc_dirs :
      #  if fastqc1 != fastqc2 :

    for nuc in avg_cont.columns :
      ax.plot(x,fl(avg_cont[nuc]),'-')

    ax.set_title("Per base sequence content")
    ax.set_ylabel('% base')
    ax.set_xlabel('Position in read (bp)')
    f.subplots_adjust(bottom=0.19)
    self.report_data["per_base_content"] = fig_to_b64_img(f)
    return self.report_data["per_base_content"]

  # Sequence Length Distribution
  def sequence_length_distribution(self) :
    met = "Sequence Length Distribution"
    f = mp.figure()
    ax = f.gca()
    xlabs = self.fastqc_modules[0][met].Length
    x = fl(np.arange(len(xlabs)))
    ax.set_xticks(x)
    ax.set_xticklabels(xlabs,rotation=90)
    ax.set_yscale('log')
    for samp,v in self.sorted_modules :
      xlabs = v[met].Length
      x = fl(np.arange(len(xlabs)))
      ls = '-' if v[met].status == 'pass' else '--'
      f.gca().plot(x,fl(v[met].Count),ls)
    ax.set_title(met)
    ax.set_ylabel('Read count')
    ax.set_xlabel('Length (bp)')    
    ax.grid()
    f.subplots_adjust(bottom=0.15)
    self.report_data["seq_len_dist"] = fig_to_b64_img(f)
    return self.report_data["seq_len_dist"]

  # Per sequence GC content
  def per_sequence_gc_content(self) :
    met = "Per sequence GC content"
    f = mp.figure()
    ax = f.gca()
    x = fl(np.arange(100))
    ax.set_xticks(np.arange(0,100,5))
    ax.set_xticklabels(np.arange(0,100,5),rotation=90)
    ax.set_yscale('log')
    for samp,v in self.sorted_modules :
      ls = '-' if v[met].status == 'pass' else '--'
      ax.plot(fl(v[met].GC_Content),fl(v[met].Count),ls)
    ax.set_title("Per sequence GC content")
    ax.set_ylabel('# reads')
    ax.set_xlabel('Mean GC content (%)')
    ax.grid()
    self.report_data["per_seq_GC"] = fig_to_b64_img(f)
    return self.report_data["per_seq_GC"]

  # Per base N content
  def per_base_n_content(self) :
    met = "Per base N content"
    f = mp.figure()
    ax = f.gca()
    xlabs = self.fastqc_modules[0]["Per base N content"].Base
    x = fl(np.arange(len(xlabs)))
    ax.set_xticks(x)
    ax.set_xticklabels(xlabs,rotation=90)

    ax.set_yscale('log')
    for samp,v in self.sorted_modules :
      ls = '-' if v[met].status == 'pass' else '--'
      ax.plot(x,fl(v[met]['N-Count']),ls)
    ax.set_title("Per base N content")
    ax.grid()
    ax.set_ylabel('% N')
    ax.set_xlabel('Position in read (bp)')
    f.subplots_adjust(bottom=0.19)
    self.report_data["per_base_N"] = fig_to_b64_img(f)
    return self.report_data["per_base_N"]

  def sequence_duplication_levels(self) :
    met = "Sequence Duplication Levels"
    f = mp.figure()
    ax = f.gca()
    ax.set_yscale('log')
    ax.set_xscale('log')
    for samp,v in self.sorted_modules :
      ls = 'o-' if v[met].status == 'pass' else 'o--'
      x = fl(v[met].Percentage_of_total)
      y = fl(v[met].Percentage_of_deduplicated)
      ax.plot(x,y,ls)
    ax.plot([0,100],[0,100],'k-')
    ax.set_title("Sequence Duplication Levels")
    ax.set_ylabel('% Deduplicated sequences')
    ax.set_xlabel('% Total sequences')
    f.subplots_adjust(bottom=0.19)
    self.report_data["seq_dup"] = fig_to_b64_img(f)
    return self.report_data["seq_dup"]

