#!/usr/bin/env python3
"""\
Combines the output from two or more fastqc output directories into an
HTML page with plots of the different modules.

Usage:
   multi_fastqc.py [options] <fastqc_dir>...

Options:
  -h --help   Show this screen.
"""
__version__ = '0.0.1'

from collections import defaultdict

import base64
from contextlib import contextmanager
import io
import os
import re
import sys

from docopt import docopt

import numpy as np

import pandas as pd

from multifastqc.parser import parse_fastqc_data
from multifastqc.plotting import MatplotlibPlotter


# modules:
# - summary.txt
# - Basic Statistics
# - Library size
# - Per base sequence quality
# - Per tile sequence quality
# - Per sequence quality scores
# - Per base sequence content
# - Per sequence GC content
# - Per base N content
# - Sequence Length Distribution
# - Sequence Duplication Levels
# - Overrepresented sequences
# - Adapter Content
# - Kmer Content

tmpl = """\
<html>
  <head>
    <style>
      span {{ font-weight: bold; }}
      .black {{ color: #000; }}
      .orange {{ color: #f88; }}
      .red {{ color: #f00; }}
      .purple {{ color: #f0f; }}
    </style>
  </head>
  <body>
    {summary}
    <hr>
    {library_size}
    <hr>
    {per_base_qual}
    <hr>
    {per_seq_qual}
    <hr>
    {per_base_content}
    <hr>
    {seq_len_dist}
    <hr>
    {per_seq_GC}
    <hr>
    {per_base_N}
    <hr>
    {seq_dup}
  </body>
</html>"""

if __name__ == "__main__" :

  args = docopt(__doc__, version=__version__)

  summary = pd.DataFrame()
  modules = {}

  # this dictionary will contain the base64 encoded plot images
  # for the html template
  report_data = {}

  for d in args["<fastqc_dir>"] :

    # strip off trailing slash if needed
    d = d[:-1] if d.endswith("/") else d

    name = os.path.basename(d)

    summary_fn = "{}/summary.txt".format(d)
    summary_df = pd.read_csv(summary_fn
      ,index_col=1
      ,names=('status','fn')
      ,delimiter="\t"
    )

    summary[name] = summary_df.status

    data_fn = "{}/fastqc_data.txt".format(d)
    modules[name] = parse_fastqc_data(data_fn)

  # the plotter class has a method for each type of plot
  # eventually there will be different types of plotters,
  # e.g. matplotlib- vs javascript d3-based
  plotter = MatplotlibPlotter(summary,modules)

  # each method stores the html img tags internally to a dictionary
  # like the report_data variable here
  plotter.summary()
  plotter.library_size()
  plotter.per_base_sequence_quality()
  plotter.per_sequence_quality_scores()
  plotter.per_base_sequence_content()
  plotter.sequence_length_distribution()
  plotter.per_sequence_gc_content()
  plotter.per_base_n_content()
  plotter.sequence_duplication_levels()

  print(tmpl.format(**plotter.report_data))
