# README #

A script that combines the output of two or more fastqc(http://www.bioinformatics.babraham.ac.uk/projects/fastqc/) results into a single page with figures capturing the various metrics. The tool accepts two or more paths to fastqc output directories on the command line and prints out to stdout an HTML page with embedded tables and images.

Currently there are plots/tables for:

* summary.txt
* Per base sequence quality
* Per sequence quality scores
* Per base sequence content
* Sequence Length Distribution

Eventually we would like to support the remaining metrics as well:

* Per sequence GC content
* Per base N content
* Sequence Duplication Levels
* Overrepresented sequences
* Adapter Content
* Kmer Content

The *Per tile sequence quality* metric is currently skipped, since it is
IO-intensive and usually not particularly valuable.

### How do I get set up? ###

The package is currently deployed in an anaconda environment on SCC. To load, run
and develop the package, run the following *from within the source directory*:

```
#!bash

$ module load anaconda
$ source activate multi_fastqc
$ . activate_dev.sh
```

This will set up your environment to make the ``multi_fastqc.py`` script available
to run as follows:

```
#!bash

$ multi_fastqc.py
Usage:
   multi_fastqc.py [options] <fastqc_dir>...
```

Changes to ``scripts/multi_fastqc.py`` and modules under ``multifastqc`` will be
immediately reflected when running the utility.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
