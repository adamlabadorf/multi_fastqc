import pytest

from multifastqc import METRICS
from multifastqc.parser import parse_fastqc_data

@pytest.fixture
def fastqc_data() :
  return parse_fastqc_data("tests/fastqc_data.txt")

def test_parse_fastqc_data(monkeypatch,fastqc_data) :

  assert len(fastqc_data) == len(METRICS)
  assert all([_ in METRICS for _ in fastqc_data])

# metric-specific tests
def test_stats(fastqc_data) :
  met = fastqc_data["Basic Statistics"]
  assert all(met.columns == ("Measure","Value"))
  assert met.shape == (7,2)

  met = fastqc_data["Per base sequence quality"]
  assert met.shape[1] == 7

  met = fastqc_data["Per tile sequence quality"]
  # this metric is skipped, so there should be nothing in the df
  assert met.shape == (0,3)

  met = fastqc_data["Per sequence quality scores"]
  assert met.shape == (3,2)

  met = fastqc_data["Per base sequence content"]
  assert met.shape == (3,5)

  met = fastqc_data["Per sequence GC content"]
  assert met.shape == (101,2)

  met = fastqc_data["Per base N content"]
  assert met.shape == (3,2)

  met = fastqc_data["Sequence Length Distribution"]
  assert met.shape == (3,2)

  met = fastqc_data["Sequence Duplication Levels"]
  assert met.shape == (16,3)

  met = fastqc_data["Overrepresented sequences"]
  assert met.shape == (3,4)

  met = fastqc_data["Adapter Content"]
  assert met.shape == (3,5)

  met = fastqc_data["Kmer Content"]
  assert met.shape == (3,5)
